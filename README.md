# Rsyslog Examples

This repository contains some examples of configuring Rsyslog to do different things.

## Remote Syslog using TLS - `remote_tls.conf`

Shows a basic configuration for collecting remote syslog via TLS and storing log files in separate
files based on the sending hostname (as resolved by DNS) and by date.

### Usage with Ubuntu 22.04

With this file, you would want to name it something like `10-remote_tls.conf` in the `/etc/rsyslog.d/`
directory. This will ensure that the configuration will catch the remote syslog messages before they
are also written to `/var/log/syslog`.

This configuration will ensure that remote and local syslog messages are properly separated.